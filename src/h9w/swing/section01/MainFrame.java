package h9w.swing.section01;

import javax.swing.JFrame;

public class MainFrame extends JFrame {

	public MainFrame() {
		super("Pokemon Run");
		this.setBounds(500, 200, 850, 600);
		new StartPage(this);
		
		
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
