package h9w.swing.section01;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class StartPage extends JPanel{

	public StartPage(MainFrame mf) {
		
		this.setBounds(500, 200, 850, 600);
		
		JPanel thisPanel = this;
		this.setLayout(null);
		
		JButton btn1 = new JButton("회원가입");
		JButton btn2 = new JButton("ID/PWD찾기");
		JButton btn3 = new JButton("로그인");
		
		btn1.setLocation(600, 100);
		btn2.setLocation(600, 200);
		btn3.setLocation(600, 300);
		
		btn1.setSize(200, 80);
		btn2.setSize(200, 80);
		btn3.setSize(200, 80);
		
		this.add(btn1);
		this.add(btn2);
		this.add(btn3);
		mf.add(this);
	}
	
	
}
